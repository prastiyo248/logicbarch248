﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Kacamata
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan jumlah uangmu ");
            int uang = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan harga kacamata ");
            string kacamata = Console.ReadLine();

            Console.WriteLine("Masukan harga baju ");
            string baju = Console.ReadLine();

            int[] kacamataArray = Utility.ConvertStringToArray(kacamata);

            int[] bajuArray = Utility.ConvertStringToArray(baju);

            if (uang < 0)
            {
                Console.WriteLine("Uangnya tidak boleh kurang nol");
            }
            else
            {
                int temp = 0;
                int maximum = 0;

                for (int i = 0; i < kacamataArray.Length; i++)
                {
                    for (int j = 0; j < bajuArray.Length; j++)
                    {
                        if (kacamataArray[i] + bajuArray[j] <= uang)
                        {
                            temp = kacamataArray[i] + bajuArray[j];

                            if (maximum < temp)
                            {
                                maximum = temp;
                            }
                        }
                    }
                }

                if (maximum == 0)
                {
                    Console.WriteLine("Dana tidak mencukupi");
                }
                else
                {
                    Console.WriteLine("Uang yang akan digunakan semaksimal mungkin ialah " + maximum);
                }
            }
        }
    }
}
