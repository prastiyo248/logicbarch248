﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Program
    {
        static void Main(string[] args)
        {
            String answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal:");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Bilangan Prima");
                        bilanganPrima.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Es loli");
                        EsLoli.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Modus");
                        Modus.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Custom sort");
                        CustomSort.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Pustaka");
                        Pustaka.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("kacamata ");
                        Kacamata.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("lilin fibonacci ");
                       LilinFibonacci.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Integer geser");
                        IntegerGeser.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Parkir");
                        Parkir.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Naik Gunung");
                        NaikGunung.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Kaos kaki");
                        KaosKaki.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Pembulatan nilai");
                        PembulatanNilai.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }
                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();
            }
        }
    }
}
