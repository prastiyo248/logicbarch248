﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logicbatch248
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input nomor soal: ");
            int nomorsoal = int.Parse(Console.ReadLine());

            if (nomorsoal == 1)
            {
                //soal perhitungan bmi
                Console.WriteLine("Masukkan berat dalam Satuan Kg");
                double berat = double.Parse(Console.ReadLine());

                Console.WriteLine("Masukkan Tinggi dalam Satuan Meter");
                double tinggi = double.Parse(Console.ReadLine());

                double bmi = berat / (tinggi * tinggi);

                Console.WriteLine("BMInya adalah " + bmi);
                Console.WriteLine();

                if (bmi < 18.5)
                {
                    Console.WriteLine("Status: underweight");
                }

                else if (bmi > 25)
                {
                    Console.WriteLine("Status: normal");
                }
                else
                {
                    Console.WriteLine("Status: overweight");
                }
            }

            else if (nomorsoal == 2)
            {
                //soal satu deret satu dimensi
                Console.WriteLine("Masukkan inputan:");
                int length = int.Parse(Console.ReadLine());

                //soal1 
                Console.WriteLine("==soal 1==");
                int[] angkaGanjilArray = new int[length];
                int angkaGanjil = 1;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {
                    angkaGanjilArray[i] = angkaGanjil;
                    angkaGanjil += 2;
                    //Console.Write(" " + angkasoal1);
                    //angkasoal1 = angkasoal1 + 2;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGanjilArray[i] + " ");
                }
                Console.WriteLine();

                //soal2 
                Console.WriteLine("==soal 2==");
                int[] angkaGenapArray = new int[length];
                int angkaGenapSoal2 = 2;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {
                    angkaGenapArray[i] = angkaGenapSoal2;
                    angkaGenapSoal2 = angkaGenapSoal2 + 2;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGenapArray[i] + " ");
                }
                Console.WriteLine();

                //soal3
                Console.WriteLine("==soal 3==");
                int[] angkaSoal3Array = new int[length];
                int angkaSoal3 = 1;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {
                    angkaSoal3Array[i] = angkaSoal3;
                    angkaSoal3 = angkaSoal3 + 3;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaSoal3Array[i] + " ");
                }

                Console.WriteLine("");

                //soal4
                Console.WriteLine("==soal 4==");
                int[] angkaSoal4Array = new int[length];
                int angkaSoal4 = 1;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {
                    angkaSoal4Array[i] = angkaSoal4;
                    angkaSoal4 = angkaSoal4 + 4;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaSoal4Array[i] + " ");
                }
                Console.WriteLine("");

                //soal5
                Console.WriteLine("==soal 5==");
                int angkaSoal5 = 1;
                for (int i = 0; i < length; i++)
                {
                    if ((i + 1) % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write(angkaSoal5 + " ");
                        angkaSoal5 = angkaSoal5 + 4;
                    }
                }
                Console.WriteLine("");

                //soal6
                Console.WriteLine("==soal 6==");
                int angkaSoal6 = 1;
                for (int i = 1; i <= length; i++)
                {

                    if (i % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write(angkaSoal6 + " ");
                    }
                    angkaSoal6 = angkaSoal6 + 4;
                }
                Console.WriteLine("");

                //soal7
                Console.WriteLine("==soal 7==");
                int[] angkaSoal7Array = new int[length];
                int angkaSoal7 = 2;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {
                    angkaSoal7Array[i] = angkaSoal7;
                    angkaSoal7 = angkaSoal7 * 2;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaSoal7Array[i] + " ");
                }
                Console.WriteLine("");

                //soal8
                Console.WriteLine("==soal 8==");
                int[] angkaSoal8Array = new int[length];
                int angkaSoal8 = 3;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {

                    angkaSoal8Array[i] = angkaSoal8;
                    angkaSoal8 = angkaSoal8 * 3;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaSoal8Array[i] + " ");
                }
                Console.WriteLine("");

                //soal9
                Console.WriteLine("==soal 9==");
                int angkaSoal9 = 4;
                for (int i = 0; i < length; i++)
                {
                    if ((i + 1) % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write(angkaSoal9 + " ");
                        angkaSoal9 = angkaSoal9 * 4;
                    }
                }

                Console.WriteLine("");

                //soal10
                Console.WriteLine("==soal 10==");
                int angkaSoal10 = 1;
                for (int i = 1; i <= length; i++)
                {
                    angkaSoal10 = angkaSoal10 * 3;
                    if (i % 4 == 0)
                    {
                        Console.Write("XXX ");
                    }
                    else
                    {
                        Console.Write(angkaSoal10 + " ");
                    }
                }
                Console.WriteLine("");

                //soal 11 
                Console.WriteLine("==soal 11==");
                int[] fibonancy = new int[length];
                fibonancy[0] = 1;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {
                    if (i <= 1)
                    {
                        fibonancy[i] = 1;
                    }
                    else
                    {
                        fibonancy[i] = fibonancy[i - 1] + fibonancy[i - 2];

                    }
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(fibonancy[i] + " ");
                }
                Console.WriteLine("");

                //soal12
                Console.WriteLine("==soal 12==");
                int[] angkaSoal12Array = new int[length];

                //memasukkan value ganjil terbalik
                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 1)
                    {
                        if (i < 1)
                        {
                            angkaSoal12Array[i] = 1;
                        }
                        else if (i <= (length / 2))
                        {
                            angkaSoal12Array[i] = angkaSoal12Array[i - 1] + 2;
                        }
                        else
                        {
                            angkaSoal12Array[i] = angkaSoal12Array[i - 1] - 2;
                        }
                    }

                 //memasukkan value genap terbalik
                    else
                    {
                        if (i < 1)
                        {
                            angkaSoal12Array[i] = 1;
                        }
                        else if (i < (length / 2))
                        {
                            angkaSoal12Array[i] = angkaSoal12Array[i - 1] + 2;
                        }
                        else if (i == (length / 2))
                        {
                            angkaSoal12Array[i] = angkaSoal12Array[i - 1];
                        }
                        else
                        {
                            angkaSoal12Array[i] = angkaSoal12Array[i - 1] - 2;
                        }
                    }
                }
                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaSoal12Array[i] + " ");
                }
                Console.WriteLine();

                //soal 13
                Console.WriteLine("==soal 13==");
                int[] fibonancy13 = new int[length];
                fibonancy[0] = 1;

                //memasukkan value
                for (int i = 0; i < length; i++)
                {
                    if (i < 3)
                    {
                        fibonancy[i] = 1;
                    }
                    else
                    {
                        fibonancy[i] = fibonancy[i - 1] + fibonancy[i - 2] + fibonancy[i - 3];

                    }
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(fibonancy[i] + " ");
                }
                Console.WriteLine();

                //soal bilangan prima
                Console.WriteLine("Soal Bilangan Prima");
                int[]bilanganPrima = new int[length];
              

                //masukkan value
                for (int i = 0; i < length; i++)
                {
                    
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    
                }
                Console.WriteLine("");
               
                //soal 15
                Console.WriteLine("==Soal 15==");
                int[] fiboArray = new int[length];
               int  fibo = 1;

                //masukkan value genap dulu baru ganjil
                for (int i = 0; i < length; i++)
                {
                    //masukkan genap
                    if (length % 2 == 0 )
                    {
                        if (i < length / 2 && i <=1)
                        {
                            
                            fiboArray[length - 1 - i] = fibo;
                            fiboArray[i] = fibo;
                           
                        }
                        else if (i < length / 2 && i > 1)
                        {
                            fiboArray[length - 1 - i] = fiboArray[i - 1] + fiboArray[i - 2];
                            fiboArray[i] = fiboArray[i - 1] + fiboArray[i - 2];
                        }
                        
                       
                    }
                    else
                    {
                        // masukkan ganjil
                        if (i < length / 2 && i <= 1) // jika 0 kurang dari panjang inputan masuknya sini dan supaya hasil fibonacy dari 1
                        {
                            fiboArray[length - 1 - i] = fibo;
                            fiboArray[i] = fibo;
                        }
                        else if (i <= length / 2 && i > 1) // jika i lebih dari panjang inputan  masuk sini dan mulai proses mirroringnya 
                        {
                            fiboArray[length - 1 - i] = fiboArray[i - 1] + fiboArray[i - 2]; // sama kaya genap
                            fiboArray[i] = fiboArray[i - 1] + fiboArray[i - 2]; // sama kaya genap
                        }


                    }
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(fiboArray[i] + " ");
                }




            }               
            else
            {
                Console.WriteLine("Nomor Soal tidak ditemukan");
            }
            Console.ReadKey();

        }
    }
}

