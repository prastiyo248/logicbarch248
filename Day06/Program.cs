﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Program
    {
        static void Main(string[] args)
        {
            String answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal:");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Kertas");
                        Kertas.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Vokal dan konsonan");
                        VokalDanKonsonan.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Tarif parkir ");
                        TarifParkir.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Belanja online");
                        BelanjaOnline.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Gambreng");
                        Gambreng.Resolve();
                        break;        
            
                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }
                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();
            }
        }
    }
}