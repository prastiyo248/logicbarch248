﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal08
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n");
            int n = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];
            

            // memasukkan value
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        array2D[i, j] = j * 2 ;                                          }
                    else
                    {
                        array2D[i, j] = j * 3;
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
