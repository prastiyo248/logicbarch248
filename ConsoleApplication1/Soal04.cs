﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal04
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan nilai n2");
            int n2 = int.Parse(Console.ReadLine());

            int[,] array2D = new int[2, n];
            int deret = 0;
            int bantu = 1;
            int bantu2 = 1;

            // memasukkan value
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else if (i >= 1)
                    {
                        if (deret % 2 == 0)
                        {
                            array2D[i, j] = bantu ;
                            bantu++;
                        }
                        else
                        {
                            array2D[i, j]= bantu2 * n2;
                            bantu2++;
                        }
                        deret++;                        
                    }                   
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
