﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal05
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai n");
            int n = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];
            int mulai = 0;

            // memasukkan value
            for (int i = 0; i < 3 ; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    array2D[i, j] = mulai;
                    mulai++;
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
