﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            String answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal:");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Camel Case");
                        CamelCase.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Strong Password");
                        StrongPassword.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Caesar chipher");
                        CaesarCipher.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Mars Exploration");
                        MarsExploration.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("HackerRank In a String");
                        HackerRankInaString.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Staircase ");
                        Pangrams.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Separate the number");
                        SeparateTheNumber.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Gemstones");
                        Gemstones.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Making anagram");
                        MakingAnagrams.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Two strings");
                        TwoStrings.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Middle Asterisk3");
                        MiddleAsterisk3.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Palindrome");
                        Palindrome.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }
                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();
            }
        }
    }
}
