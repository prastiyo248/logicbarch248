﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class StrongPassword
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan password yang kuat");
            string password = Console.ReadLine();
            char[] passwordChar = password.ToCharArray();

            int cekLower = 0;
            int cekUpper = 0;
            int cekSpec = 0;
            int cekNumber = 0;
            int total = 0;

            for (int i = 0; i < passwordChar.Length; i++)
            {
                //jika ada karakter yang huruf kecil maka syarat terpenuhi
                if (char.IsLower(passwordChar[i]))
                {
                    cekLower += 1;
                }
                //jika ada karakter yang huruf besar maka syarat terpenuhi
                if (char.IsUpper(passwordChar[i]))
                {
                    cekUpper += 1;
                }
                //jika ada spesial karakter maka syarat terpenuhi
                if (!char.IsLetterOrDigit(passwordChar[i]))
                {
                    cekSpec += 1;
                }
                //jika ada angka maka syarat terpenuhi
                if (char.IsNumber(passwordChar[i]))
                {
                    cekNumber += 1;
                }
                //
                if (passwordChar.Length < 6)
                {
                    Console.WriteLine("Minimal panjang password adalah 6 digit, Mohon Coba Kembali!");
                }
            }
            if (cekLower < 1)
            {
                total++;
            }
            if (cekUpper < 1)
            {
                total++;
            }
            if (cekSpec < 1)
            {
                total++;
            }
            if (cekNumber < 1)
            {
                total++;
            }

            Console.Write(total + " ");
            Console.WriteLine();

        }
    }
}
