﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Pangrams
    {
        public static void Resolve()
        {
            //input kalimat
            System.Console.WriteLine("Masukkan kalimat");
            string kalimatAwal = System.Console.ReadLine();
            string kalimatLower = kalimatAwal.ToLower();
            string kumpulanHuruf = "abcdefghijklmnopqrstuvwxyz";

            int jumlah = 0;

            //perulangan cek apakah kalimat mengandung a - z
            for (int i = 0; i < kumpulanHuruf.Length; i++)
            {
                for (int j = 0; j < kalimatLower.Length; j++)
                {
                    if (kumpulanHuruf[i] == kalimatLower[j])
                    {
                        jumlah++;
                        break;
                    }
                }
            }

            //jika jumlah = 26 maka pangram sesuai jumlah abjad
            if (jumlah == 26)
            {
                System.Console.WriteLine("Kalimat Termasuk Pangrams");
            }

            else
            {
                System.Console.WriteLine("Kalimat Bukan Termasuk Pangrams");
            }
            }
        }
    }

