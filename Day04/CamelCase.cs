﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CamelCase
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan kalimat:");
            string kalimat = Console.ReadLine();

            char[] kalimatChar = kalimat.ToCharArray();
            int totalKalimat = 1;

            if (char.IsLower(kalimatChar[0]))
            {
                for (int i = 0; i < kalimatChar.Length; i++)
                {

                    if (char.IsUpper(kalimatChar[i]))
                    {
                        totalKalimat += 1;
                    }

                }
                Console.WriteLine(totalKalimat);
               
            }
            else
            {
                Console.WriteLine("Bukan Camel Case");
            }          
        }
    }
}
