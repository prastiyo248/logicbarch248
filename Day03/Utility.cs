﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Utility
    {
        public static int Sum(int number1, int number2)
        {
            int total = number1 + number2;
            return total;
        }

        public static int[] ConvertStringToArray(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            //convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }
            return numbersArray;
        }

        public static long[] ConvertStringToArrayLong(string longnumbers)
        {
            string[] longstringNumbersArray = longnumbers.Split(' ');
            long[] longnumbersArray = new long [longstringNumbersArray.Length];

            //convert to int
            for (int i = 0; i < longnumbersArray.Length; i++)
            {
                longnumbersArray[i] = long.Parse(longstringNumbersArray[i]);
            }
            return longnumbersArray;
        }


        public static void PrintArray2D(int[,] array2D)
        {
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    Console.Write(array2D[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

    }
}
