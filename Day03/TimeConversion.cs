﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class TimeConversion
    {
        public static void Resolve()
        {
            Console.WriteLine("input the time:");
            string time = Console.ReadLine();
            
            if (!time.Contains("AM") && !time.Contains("PM"))
            {
                Console.WriteLine("Format yang dimasukkan salah");
            }
            else
            {
                try
                {
                    DateTime dateTime = Convert.ToDateTime(time);
                    Console.Write("format waktu dalam 24 jam adalah:");
                    Console.WriteLine(dateTime.ToString("HH:mm:ss"));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Format yang anda masukkan salah");
                }
            }

        }
    }
}
