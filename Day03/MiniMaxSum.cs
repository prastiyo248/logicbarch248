﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class MiniMaxSum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number:");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToArray(numbers);
            int min = 0;
            int max = 0;
            int all = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                all += numbersArray[i];
            }

            min = all - numbersArray.Max();
            max = all - numbersArray.Min();
            Console.WriteLine(min.ToString() + " " + max.ToString());
        }
    }
}
