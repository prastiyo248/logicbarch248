﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifference
    {
        public static void Resolve()
        {
            Console.WriteLine("Input the length of matrix:");
            int length = int.Parse(Console.ReadLine());

            int[,] array2D = new int[length, length];
            int diagonalPrimer = 0 ;
            int diagonalSekunder = 0;

            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter the " + (i + 1) + "set of number");
                string numbers = Console.ReadLine();

                int[] array = Utility.ConvertStringToArray(numbers);
                if (array.Length != length)
                {
                    Console.WriteLine("Wrong numbers, try again");
                    i = -1;
                }
                else
                {
                    for (int j = 0; j < length; j++)
                    {
                        array2D[i, j] = array[j];
                    }
                }
            }

            for (int i = 0; i < length; i++)
            {
                diagonalPrimer += array2D[i, i];
                diagonalSekunder += array2D[i, length - i - 1];
            }

            Console.WriteLine();
            Utility.PrintArray2D(array2D);
            Console.WriteLine("Diagonal primer: " + diagonalPrimer);
            Console.WriteLine("Diagonal sekunder: " + diagonalSekunder);

            int diagonalDifference = diagonalPrimer - diagonalSekunder;
            Console.WriteLine("Diagonal difference : " + Math.Abs(diagonalDifference));
        }     
    }
}
