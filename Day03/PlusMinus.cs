﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class PlusMinus
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number:");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToArray(numbers); 
            int positif = 0;
            int negative = 0;
            int zero = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] > 0 )
                {
                    ++positif;
                }
                else if (numbersArray[i] < 0)
                {
                    ++negative;
                }
                else
                {
                    ++zero;
                }
            }
            Console.WriteLine("Positif: "+(double)positif / numbersArray.Length);
            Console.WriteLine("negatif: "+(double)negative / numbersArray.Length);
            Console.WriteLine("zero: "+(double)zero / numbersArray.Length);
            
        }
    }
}
