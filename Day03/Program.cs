﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            String answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal:");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Solve me first");
                        SolveMeFirst.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Time conversion");
                        TimeConversion.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Simple array sum");
                        SimpleArraySum.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Diagonal difference");
                        DiagonalDifference.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Plus Minus");
                        PlusMinus.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Staircase ");
                        Staircase.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Mini Max ");
                        MiniMaxSum.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Birthday cake candles");
                        BirthdayCakeCandles.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Very big sum");
                        VeryBigSum.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Compare the triplet");
                        CompareTheTriplet.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }
                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();
            }
        }
    }
}
