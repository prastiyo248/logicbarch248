﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest
{
    class Program
    {
        static void Main(string[] args)
        {
            String answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal:");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Soal Nomor 1 Transaksi beli pulsa");
                        Soal1TransaksiPulsa.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Pengantar makanan");
                        PengantarMakanan.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Konversi Volume ");
                        Soal3KonversiVolume.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Kopi");
                        Kopi.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Porsi makan");
                        Soal5PorsiMakan.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Game");
                        Game.Resolve();
                        break;


                    case 7:
                        Console.WriteLine("Keranjang buah");
                        KeranjangBuah.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Atm");
                        Atm.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Main kartu");
                        MainKartu.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Panjang deret");
                        PanjangDeret.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("EviternityNumbers");
                        EviternityNumbers.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }
                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();
            }
        }
    }
}